# Weather Web App

Wheather App is a web application that allows users to see various weather metrics on a timeline. And administrators can add these metrics.

### Quick links

- [API Doc (Swagger)](https://weather-api-factorial-bcfe94683491.herokuapp.com/api)
- [Try the App](https://wheather-front.vercel.app/)

### Project structure

The UI is split into components with top-down data flow. All charts share common Filter component to set up the data rage and time interval for fetching and rendering datasets. The MetricList loads all metrics and use MetricValues for rendering charts for each metric.

There are 2 modes: read-only and admin. To be able to add new metric values one should login with admin credentials:

- login: admin
- password: 12345

In admin mode there are plus buttons available to trigger AddDataForm modal pop-up to fill in new metric values data and adding them to database via API call. It's possible to add a brand new metric if it is not showed in the list.

### Decisions made

- To organize top-down data flow from main App component to MetricList then to MetricValues and Chart. In this way we keep data flow simple without a need of introducing a state management tool such as Redux.
- Some states are moved to higher level using "lifting state up" technique.
- To make styling for Desktop version with minimum adoptivity to mobile.

### Possible improvements

- To add preloaders
- To add form validation for value submition
- To handle error status codes when call API
- To set a limit of data ranges depending on choosen interval. For example, when interval is set to "Minute" shorten the period to minimum of a day with option to scroll the chart and dynamic data loading from backend (like in Trading charts).
- To implement adoptive mobile version using media queries.