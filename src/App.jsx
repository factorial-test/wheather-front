import React, { useState, useEffect } from 'react';
import NavBar from './components/NavBar/NavBar';
import MetricList from './components/MetricList/MetricList';
import Filter from './components/Filter/Filter';

const App = () => {
  const [loggedIn, setLoggedIn] = useState(false);
  const [fromDate, setFromDate] = useState('2023-01-01');
  const [toDate, setToDate] = useState('2024-02-01');
  const [interval, setInterval] = useState('Day');
  const [activeFromDate, setActiveFromDate] = useState(fromDate);
  const [activeToDate, setActiveToDate] = useState(toDate);
  const [activeInterval, setActiveInterval] = useState(interval);

  useEffect(() => {
    const accessToken = localStorage.getItem('accessToken');
    if (accessToken) {
      setLoggedIn(true);
    }
  }, []);

  const handleLogin = (accessToken) => {
    setLoggedIn(true);
    localStorage.setItem('accessToken', accessToken)
  };

  const handleLogout = () => {
    setLoggedIn(false);
  };

  const onFilterApply = () => {
    setActiveFromDate(fromDate);
    setActiveToDate(toDate);
    setActiveInterval(interval);
  }

  return (
    <div className='container'>
      <div className='header' style={{ position: 'sticky', top: 0, background: 'white' }}>
        <NavBar onLogin={handleLogin} onLogout={handleLogout} loggedIn={loggedIn} />
        <Filter
          fromDate={fromDate}
          setFromDate={setFromDate}
          toDate={toDate}
          setToDate={setToDate}
          interval={interval}
          setInterval={setInterval}
          onFilterApply={onFilterApply}
        />
      </div>
      <MetricList
        fromDate={activeFromDate}
        toDate={activeToDate}
        interval={activeInterval}
        isAdmin={loggedIn}
      />
    </div>
  )
}

export default App;