import React from 'react';
import './Chart.css';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  Colors,
} from 'chart.js';
import { Line } from 'react-chartjs-2';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  Colors
);

const Chart = ({ metric = '', dataValues = [] }) => {
  const chartData = {
    labels: dataValues.map(([label,]) => label),
    datasets: [{
      label: metric,
      data: dataValues.map(([, value]) => value),
      borderWidth: 2,
    }]
  }

  const options = {
    responsive: true,
    maintainAspectRatio: false,
    elements: {
      point: {
        radius: 0
      }
    }
  }

  return (
    <div className="chart-container">
      <div style={{height: '300px'}}>
        <Line data={chartData} options={options} />
      </div>
    </div>
  );
};

export default Chart;
