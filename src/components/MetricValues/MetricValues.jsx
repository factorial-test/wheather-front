import React, { useEffect, useState } from 'react';
import Chart from '../Chart/Chart';
import './MetricValues.css';
import Api from '../../services/api/api';

const MetricValues = ({ metric, fromDate, toDate, interval, lastUpdate }) => {
  const [dataValues, setDataValues] = useState([]);
  
  useEffect(() => {
    Api.getValues(metric.metricId, fromDate, toDate, interval)
      .then(values => setDataValues(values));
  }, [fromDate, toDate, interval, lastUpdate])

  return (
    <div className="chart-container">
      <Chart metric = {metric.name} dataValues = {dataValues}/>
    </div>
  );
};

export default MetricValues;
