import React, { useState } from 'react';
import './AuthForm.css';
import Api from '../../services/api/api';

const AuthForm = ({ onLogin, onClose }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  const handleLogin = () => {
    setError('');

    Api.login(username, password)
      .then(({accessToken}) => {
        onLogin(accessToken);
      })
      .catch(() => {
        setError('Invalid username or password');
      });
  };

  return (
    <div className="auth-form">
      <div className="close-btn" onClick={onClose}>&times;</div>
      <h2>Login</h2>
      <label htmlFor="username">Username:</label>
      <input
        type="text"
        id="username"
        value={username}
        onChange={(e) => setUsername(e.target.value)}
      />
      <label htmlFor="password">Password:</label>
      <input
        type="password"
        id="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      {error ? <p style={{color:'red'}}>{error}</p> : ''}
      <button onClick={handleLogin}>Login</button>
    </div>
  );
};

export default AuthForm;
