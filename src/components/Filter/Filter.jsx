import React from 'react';
import './Filter.css';

const Filter = ({ fromDate, setFromDate, toDate, setToDate, interval, setInterval, onFilterApply }) => {
  return (
    <div className="filter-container">
      <div>
        <label htmlFor="fromDate">From:</label>
        <input
          type="date"
          id="fromDate"
          value={fromDate}
          onChange={(e) => setFromDate(e.target.value)}
        />
      </div>
      <div>
        <label htmlFor="toDate">To:</label>
        <input
          type="date"
          id="toDate"
          value={toDate}
          onChange={(e) => setToDate(e.target.value)}
        />
      </div>
      <div>         
        <label htmlFor="interval">Interval:</label>
        <select
          id="interval"
          value={interval}
          onChange={(e) => setInterval(e.target.value)}
        >
          <option value="day">Day</option>
          <option value="hour">Hour</option>
          <option value="minute">Minute</option>
        </select>
      </div>
 
      <button onClick={onFilterApply}>Apply Filter</button>
    </div>
  );
};

export default Filter;
