import React, { useEffect, useState } from 'react';
import Modal from 'react-modal';
import './AddDataForm.css';
import Api from '../../services/api/api';

const ADD_NEW_METRIC = 'ADD_NEW_METRIC';

const AddDataForm = ({ isOpen, onClose, curMetric, allMetrics }) => {
  const [metricId, setMetricId] = useState(curMetric?.metricId);
  const [value, setValue] = useState(0);
  const [timestamp, setTimestamp] = useState(new Date());
  const [newMetricName, setNewMetricName] = useState('');

  useEffect(() => {
    setMetricId(curMetric?.metricId);
    setValue(0);
    setTimestamp(new Date());
    setNewMetricName('');
  }, [curMetric])


  const handleSubmit = async (e) => {
    e.preventDefault();

    let submitMetricId = metricId;

    if (metricId === ADD_NEW_METRIC && !!newMetricName) {
      const newMetric = await Api.createMetric(newMetricName);
      submitMetricId = newMetric.metricId;
      setMetricId(metricId);
    }

    await Api.addValue(submitMetricId, timestamp, value);

    setMetricId('');
    setValue('');
    setTimestamp(new Date());

    onClose(true);
  };

  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onClose}
      contentLabel="Add Data"
      className="modal"
      overlayClassName="overlay"
    >
      <div className="modal-container">
        <div className="close-btn" onClick={onClose}>&times;</div>
        <h2>Add Observation</h2>
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label>Metric:</label>
            <select
              value={metricId}
              onChange={(e) => setMetricId(e.target.value)}
              className="form-control"
            >
              {allMetrics.map(metric => (
                <option key={metric.metricId} value={metric.metricId}>
                  {metric.name}
                </option>
              ))}
              <option value={ADD_NEW_METRIC}>Add New Metric</option>
            </select>
            {metricId === ADD_NEW_METRIC && (
              <input
                type="text"
                placeholder="New Metric"
                value={newMetricName}
                onChange={(e) => setNewMetricName(e.target.value)}
                className="form-control"
              />
            )}
          </div>

          <div className="form-group">
            <label>Value:</label>
            <input
              type="number"
              step="0.01"
              value={value}
              onChange={(e) => setValue(e.target.value)}
              className="form-control"
            />
          </div>

          <div className="form-group">
            <label>Timestamp:</label>
            <input
              type="datetime-local"
              value={timestamp.toISOString().slice(0, 16)}
              onChange={(e) => setTimestamp(new Date(e.target.value))}
              className="form-control"
            />
          </div>

          <button type="submit" className="btn-submit">
            Add
          </button>
        </form>
      </div>
    </Modal>
  );
};

export default AddDataForm;
