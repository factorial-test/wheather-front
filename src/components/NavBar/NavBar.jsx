import React, { useState } from 'react';
import AuthForm from '../AuthForm/AuthForm';
import './NavBar.css';

const NavBar = ({ onLogin, onLogout, loggedIn }) => {
  const [showAuthForm, setShowAuthForm] = useState(false);

  const handleToggleAuthForm = () => {
    setShowAuthForm(!showAuthForm);
  };

  const handleLogin = (accessToken) => {
    // Handle login logic, e.g., storing the access token in local storage
    localStorage.setItem('accessToken', accessToken);
    onLogin(accessToken);
    setShowAuthForm(false);
  };

  const handleLogout = () => {
    // Handle logout logic, e.g., removing the access token from local storage
    localStorage.removeItem('accessToken');
    onLogout();
  };

  return (
    <div className="auth">
      <h1>Wheather App ☀️ </h1>
      <div className="auth-header">
        {loggedIn ? (
          <button onClick={handleLogout}>Logout</button>
        ) : (
          !showAuthForm && <button onClick={handleToggleAuthForm}>Login</button>
        )}
      </div>

      {showAuthForm && (
        <AuthForm onLogin={handleLogin} onClose={handleToggleAuthForm} />
      )}
    </div>
  );
};

export default NavBar;
