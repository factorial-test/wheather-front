import React, { useEffect, useState } from 'react';
import './MetricList.css';
import Api from '../../services/api/api';
import MetricValues from '../MetricValues/MetricValues';
import AddDataForm from '../AddDataForm/AddDataForm';

const MetricList = ({ fromDate, toDate, interval, isAdmin }) => {
  const [metrics, setMetrics] = useState([]);
  const [curMetric, setCurMetric] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [lastUpdate, setLastUpdate] = useState(new Date());

  useEffect(() => {
    Api.listMetrics()
      .then(metrics => setMetrics(metrics));
  }, [lastUpdate]);

  const openModal = (metric) => {
    setCurMetric(metric);
    setIsModalOpen(true);
  }

  const closeModal = (withUpdate) => {
    setIsModalOpen(false);
    if (withUpdate === true) {
      setLastUpdate(new Date());
    }
  };

  return (
    <div className="metric-list">
      {metrics.map(metric => (
        <div key={metric.metricId} style={{ paddingTop: '10px' }}>
          <div style={{ display: 'flex' }}>
            <b>{metric.name}</b>
            {isAdmin ? <button
              className='circle-button'
              onClick={() => openModal(metric)}
            >+</button> : ''}
          </div>

          <MetricValues
            metric={metric}
            fromDate={fromDate}
            toDate={toDate}
            interval={interval}
            lastUpdate={lastUpdate}
          />
        </div>
      ))}
      <AddDataForm
        isOpen={isModalOpen}
        onClose={closeModal}
        allMetrics={metrics}
        curMetric={curMetric}
      />
    </div>
  );
};

export default MetricList;
