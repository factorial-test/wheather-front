import axios from "axios";

export const baseUrl = process.env.REACT_APP_API_BASE_URL ?? 'http://localhost:3000';

export default class Api {
  static listMetrics() {
    return axios.get(`${baseUrl}/api/metrics`)
      .then(res => res.data)
      .catch(error => console.error('Error fetching metrics:', error));
  }

  static getValues(metricId, from, to, interval) {
    return axios.get(`${baseUrl}/api/metrics/${metricId}/values`, {
      params: {
        from,
        to,
        interval
      }
    })
      .then(res => res.data)
      .catch(error => console.error('Error fetching metrics:', error));
  }

  static login(username, password) {
    return axios.post(`${baseUrl}/api/auth/login`, {
      username,
      password,
    })
      .then(res => res.data)
  }

  static createMetric(name) {
    return axios.post(`${baseUrl}/api/metrics`, {
      name
    }, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
      }
    })
      .then(res => res.data)
  }

  static addValue(metricId, timestamp, value) {
    return axios.post(`${baseUrl}/api/metrics/${metricId}/values`, [{
      timestamp,
      value
    }], {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
      }
    })
      .then(res => res.data)
  }
}
